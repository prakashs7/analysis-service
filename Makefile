version=`cat VERSION`
VERSION=$(version)
MODELS_PATH=analyzer/analyzers/models/
VENV_NAME=analyzer-${VERSION}
VENV_ACTIVATE=. ${VENV_NAME}/bin/activate
DOWNLOAD_MODELS=aws s3 sync s3://dragontail-ml-models/analysis_service_models/$(VERSION) $(MODELS_PATH)

download-models:
	sh -c "${VENV_ACTIVATE}; $(DOWNLOAD_MODELS)"

install: download-models
	pip install .

uninstall:
	pip freeze | grep analyzer | xargs pip uninstall -y

runserver:
	sh -c "${VENV_ACTIVATE}; start-service.sh"

unittest:
	sh -c "${VENV_ACTIVATE}; py.test"

integration-test:
	sh -c "${VENV_ACTIVATE}; ./run_integration_test.sh"

add-tensorflow-requirement:
	./add_tensorflow_requirement.sh

install-python-requirements: add-tensorflow-requirement
	test -d ${VENV_NAME} || virtualenv -p python2.7 ${VENV_NAME}; echo creating virtual environment ${VENV_NAME};
	sh -c "${VENV_ACTIVATE}; pip install -r requirements.txt"

install-awscli:
	test -d ${VENV_NAME} || virtualenv -p python2.7 ${VENV_NAME}; echo creating virtual environment ${VENV_NAME};
	sh -c "${VENV_ACTIVATE}; pip install awscli";

rm-venv:
	rm -rf $(VENV_NAME)

fresh-install: rm-venv install-awscli install-python-requirements download-models
	sh -c "${VENV_ACTIVATE}; pip install .";

