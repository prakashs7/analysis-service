import os
import logging

import argparse
import glob
import json
import pandas as pd
import requests
import uuid

from color_utils.annotation_parser import read_soft_annotation


PORT = 5000
SERVER_URL = "http://0.0.0.0:{port}".format(port=PORT)
FINAL_URL = "{server_url}/{rule}".format(server_url=SERVER_URL,
                                         rule='enqueueAnalysis')


def jaccard(set_1, set_2):
    if (len(set_1) == 0) and (len(set_2) == 0):
        return 1.0
    intersection = set_1 & set_2
    union = set_1 | set_2
    return len(intersection) / float(len(union))


def dice(jacc):
    return 2*jacc/(1 + jacc)


def analyze_file(file_name, url=FINAL_URL):
    with open(file_name, 'rb') as stream:
        files = {'file': stream}
        headers = dict(service='analyze', sessionId=uuid.uuid1())
        return requests.post(url, data=headers, files=files)


parser = argparse.ArgumentParser()
parser.add_argument("input_dir", help="directory with images to analyze")
parser.add_argument("--output_dir", help="directory for the output JSON")
parser.add_argument("--json",  action='store_true',
                    help="look for soft annotation and compare JSON")
args = parser.parse_args()

input_dir = args.input_dir
output_dir = args.output_dir if args.output_dir else args.input_dir
if not os.path.exists(output_dir):
    os.makedirs(output_dir)
    logging.info("Created output directory %s" % output_dir)


file_names = glob.glob(input_dir + '/*.jpg')
logging.info("analyzing %s images from %s" % (len(file_names), input_dir))
print("analyzing %s images from %s" % (len(file_names), input_dir))

rows = []
for file_name in file_names:
    try:
        response = analyze_file(file_name)
    except Exception as e:
        logging.error("Failed to analyze %s" % file_name)
        print(e)
        continue

    response_json = json.loads(response.text)
    json_path = os.path.join(output_dir,
                             response_json['analysis']['item_id'] + '.json')
    print('writing json to %s' % json_path)
    with open(json_path, 'w') as f:
        json.dump(response_json, f, indent=4, sort_keys=True)

    if args.json:
        annotation_name = file_name.replace('.jpg', '_json.txt')
        try:
            true_toppings = set(read_soft_annotation(annotation_name))
        except Exception as exc:
            print(file_name, exc)

        predicted_toppings = set(map(lambda x: x['item'],
                                     response_json['analysis']['toppings']))
        false_pos = predicted_toppings - true_toppings
        false_neg = true_toppings - predicted_toppings
        true_pos = true_toppings & predicted_toppings
        _jaccard = jaccard(true_toppings, predicted_toppings)
        rows.append([file_name, true_pos, false_neg, false_pos,
                     _jaccard, dice(_jaccard)])

if args.json:
    compare_file = os.path.join(args.output_dir, 'compare.csv')
    data = pd.DataFrame(rows, columns=['file_name', 'true_pos', 'false_neg',
                                       'false_pos', 'jaccard', 'dice'])
    data.to_csv(compare_file, index=False)
    print("Writing report to %s " % compare_file)
