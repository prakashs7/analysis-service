import os

from unittest import TestCase

from skimage.io import imread

from analyzer.analyzers import Decider, Segmenter
from analyzer.reporter import AnalysisResults
from color_utils.pizza_colors import PizzaClasses


PIZZA_CLASSES = PizzaClasses.default().filter('name', 'Unknown')


class TestBasicRun(TestCase):
    def setUp(self):
        self.all_toppings_names = set(PIZZA_CLASSES.get_toppings().names)
        image_file_name = '4PLYJr5-orig.jpg'
        local_path = os.path.dirname(__file__)
        image_file_path = os.path.join(local_path, image_file_name)
        image = imread(image_file_path)
        self.item = AnalysisResults()
        self.item.analyzers = ['Segmenter', 'Decider']
        self.item.analysis['im'] = image[..., :3]

    def test_segmenter_and_decider(self):
        segmenter = Segmenter()
        decider = Decider()
        self.item = segmenter.process(self.item)
        expected_shape_labels = self.item.analysis['im'].shape[0:2]
        self.assertEquals(self.item.analysis['labels'].shape,
                          expected_shape_labels)
        n_channels = PIZZA_CLASSES.num_classes
        expected_shape_probabilities = expected_shape_labels + (n_channels, )
        self.assertEquals(self.item.analysis['class_probabilities'].shape,
                          expected_shape_probabilities)
        self.item = decider.process(self.item)
        toppings = self.item.reportables['toppings'].get()
        topping_names = [topping['item'] for topping in toppings]
        self.assertTrue(set(topping_names) <= self.all_toppings_names)
