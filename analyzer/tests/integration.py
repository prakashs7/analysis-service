from __future__ import print_function

import logging
import os
import sys


import json
import requests

from requests.packages.urllib3.util import Retry


PORT = 5000
SERVER_URL = "http://0.0.0.0:{port}".format(port=PORT)


def get_session(max_retreis=10, backoff_factor=0.1):
    session = requests.Session()
    http_retries = Retry(connect=max_retreis, backoff_factor=backoff_factor)
    http_adapter = requests.adapters.HTTPAdapter(max_retries=http_retries)
    session.mount('http://', http_adapter)
    return session


def post_analysis_request(image_file_name='4PLYJr5-orig.jpg'):
    local_path = os.path.dirname(__file__)
    image_file_path = os.path.join(local_path, image_file_name)
    with open(image_file_path, 'rb') as stream:
        files = {'file': stream}
        headers = dict(service='analyze', sessionId='bar')
        url = "{}/{}".format(SERVER_URL, 'enqueueAnalysis')
        response = requests.post(url, data=headers, files=files)
    return response


def keys_identical(keys, reference_keys):
    return set(keys) == set(reference_keys)


def keys_is_subset(keys, reference_keys):
    return set(keys).issubset(set(reference_keys))


def verify_content_structure(content):
    """ Partial verification of the structure of the reportable JSON

    Simply testing that each nested component have the expected keys.

    Args:
        content (dict): content of the json reportable

    Returns:
        str: PASS/FAIL
        str: the reason
    """

    result = 'FAIL'

    if not keys_identical(content.keys(), ['sessionId', 'analysis']):
        return result, "content.keys != {'sessionId', 'analysis'}"

    analysis_keys = """
    burnt
    burnt_score
    divisionPenalty
    doughtype
    item_id
    labels
    labels_path
    original
    original_path
    quarterImage
    quarterImage_path
    quarters
    pizzaGrade
    recipe
    seg
    seg_path
    timestamps
    toppings
    toppings_scores
    runtimes""".split()

    # `recipe` is optional
    analysis = content['analysis']
    if not keys_is_subset(analysis.keys(), analysis_keys):
        return (result,
                "content['analysis'].keys is not a subset of the expected "
                "keys")

    toppings = analysis['toppings']
    if len(toppings) > 0:
        if not keys_identical(toppings[0].keys(), ['fill', 'item']):
            return result, "each topping should have {'fill', 'item'} as keys"

    toppings_scores = analysis['toppings_scores']
    if len(toppings_scores) > 0:
        if not keys_identical(toppings_scores[0].keys(),
                              ['area', 'item', 'score']):
            return (result,
                    "toppings_score should have {'area', 'item', 'score'}"
                    " as keys")

    pizza_grade = analysis['pizzaGrade']

    if len(pizza_grade) > 0:
        if not keys_identical(pizza_grade.keys(),
                              ['finalGrade', 'gradeBreakdown',
                               'gradeMaxValues', 'remake',
                               'remakeReasons', 'remakeThreshold']):
            return (result,
                    "pizzaGrade should have {'finalGrade', 'gradeBreakdown', "
                    "'gradeMaxValues', 'remake', 'remakeReasons', "
                    "'remakeThreshold'} as keys")
    else:
        return (result,
                "pizzaGrade empty. should have {'finalGrade', "
                "'gradeBreakdown', 'gradeMaxValues', 'remake', "
                "'remakeReasons'} as keys")

    if len(pizza_grade['gradeBreakdown']) > 0:
        if not keys_identical(pizza_grade['gradeBreakdown'].keys(),
                              ['border', 'cheese', 'spread']):
            return (result,
                    "pizza_grade - gradeBreakdown should have {'border', "
                    "'cheese', 'spread' as keys")
    else:
        return (result,
                "pizza_grade - gradeBreakdown empty. should have {'border', "
                "'cheese', 'spread' as keys")

    if len(pizza_grade['gradeMaxValues']) > 0:
        if not keys_identical(pizza_grade['gradeBreakdown'].keys(),
                              ['border', 'cheese', 'spread']):
            return (result,
                    "pizza_grade - gradeBreakdown should have {'border', "
                    "'cheese', 'spread' as keys")
    else:
        return (result,
                "pizza_grade - gradeMaxValues empty. should have {'border', "
                "'cheese', 'spread' as keys")

    grade_breakdown = pizza_grade['gradeBreakdown']
    grade_max = pizza_grade['gradeMaxValues']
    if (grade_breakdown['border'] > grade_max['border']) | \
            (grade_breakdown['cheese'] > grade_max['cheese']) | \
            (grade_breakdown['spread'] > grade_max['spread']):
        return (result,
                "pizza grade components cannot be higher than max values")

    timestamps = content['analysis']['timestamps']
    if 'created_at_utc' not in timestamps:
        return result, "timestamps must have `created_at_utc` as a key"

    return 'PASS', ''


try:
    session = get_session(max_retreis=10)
    logging.info("Trying to connect to server...")
    response = session.post("{server_url}/{rule}".format(server_url=SERVER_URL,
                                                         rule='isAlive'))
    logging.info("Server is alive")
except requests.exceptions.ConnectionError as e:
    logging.error('Cannot connect to server... Quitting')
    exit(1)


# send an image for analysis via HTTP post
test_result = 'FAIL'
try:
    response = post_analysis_request()
    print("\t get response")
    content = json.loads(response.text)
    print("\t json loaded")
    test_result, reason = verify_content_structure(content)
    if reason:
        print("JSON verification failed due to %s" % reason)
    print("\t content verified")
except Exception as e:
    print(e)


print('=' * 50)
print("\n   Integration test %s\n" % test_result)
print('=' * 50)
if test_result == 'PASS':
    sys.exit(0)
else:
    sys.exit(1)
