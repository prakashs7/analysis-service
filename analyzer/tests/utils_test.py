""" Tests for utils"""
import numpy as np

from skimage.morphology import binary_dilation, square

from analyzer import utils
from color_utils.pizza_colors import PizzaClasses

PIZZA_CLASSES = PizzaClasses.default()


def get_labels():
    params = dict()
    params['width'] = 40
    params['height'] = 40
    labels = np.zeros((params['height'], params['width']))

    cheese_label = PIZZA_CLASSES.select('name', 'Regular Cheese').labels[0]
    params['cheese_width'] = 20
    params['cheese_height'] = 20
    labels[10:30, 10:30] = cheese_label

    mask = labels == cheese_label
    params['crust_thickness'] = 2
    square_size = params['crust_thickness'] + 1
    crust_sel = binary_dilation(mask, square(square_size)) & ~mask
    labels[crust_sel] = PIZZA_CLASSES.select('name', 'Crust').labels[0]

    params['onion_width'] = 5
    params['onion_height'] = 5
    onion_x = slice(15, 15 + params['onion_width'])
    onion_y = slice(15, 15 + params['onion_height'])
    labels[onion_x, onion_y] = PIZZA_CLASSES.select('name', 'Onion').labels[0]

    params['beef_width'] = 4
    params['beef_height'] = 4
    beef_x = slice(21, 21 + params['beef_width'])
    beef_y = slice(21, 21 + params['beef_height'])
    labels[beef_x, beef_y] = PIZZA_CLASSES.select('name', 'Beef').labels[0]
    return labels, params


def test_pizza_area():
    labels, params = get_labels()
    crust_width = params['crust_thickness']
    height = params['cheese_height'] + crust_width
    width = params['cheese_width'] + crust_width
    expected_area = height * width
    assert utils.pizza_area(labels) == expected_area


def test_topping_area():
    labels, params = get_labels()
    beef_area = utils.topping_area(labels, 'Beef', relative=False)
    expected_area = params['beef_height'] * params['beef_width']
    assert beef_area == expected_area


def test_topping_mask():
    labels, params = get_labels()
    mask_beef = utils.topping_mask(labels, 'Beef')
    mask_expected = np.zeros((params['height'], params['width']), bool)
    mask_expected[21:25, 21:25] = True
    assert (mask_expected == mask_beef).all()


def test_calculate_center_mass():
    mask = np.zeros((5, 5), int)
    assert (utils.calculate_center_mass(mask) == np.array([2, 2])).all()

    mask[1, 1] = 1
    expected_center = np.array([1, 1])
    assert (utils.calculate_center_mass(mask) == expected_center).all()

    mask[1, 3] = 1
    expected_center = np.array([1, 2])
    assert (utils.calculate_center_mass(mask) == expected_center).all()


def test_calculate_mask_center():
    mask_even = np.zeros((4, 4))
    res_1 = utils.calculate_mask_center(mask_even)
    assert (res_1 == np.array([2, 2])).all()

    mask_off = np.zeros((5, 5))
    res_2 = utils.calculate_mask_center(mask_off)
    assert (res_2 == np.array([2, 2])).all()


def test_keep_main_cluster():
    # TEST1: main cluster touches the boundary
    # - contains clusters touching the boundary, and not touching
    # 1 0 1 1 0     0 0 0 0 0
    # 0 0 0 0 0     0 0 0 0 0
    # 0 1 0 0 1 --> 0 0 0 0 1
    # 0 1 0 1 1     0 0 0 1 1
    # 0 0 0 0 0     0 0 0 0 0

    mask_blobs_1 = np.zeros((5, 5), np.int)
    mask_blobs_1[0, 0] = 1
    mask_blobs_1[0, 2:4] = 1
    mask_blobs_1[2:4, 1] = 1
    mask_blobs_1[2:4, 4] = 1
    mask_blobs_1[3, 3] = 1
    mask_main_cluster_1 = utils.keep_main_cluster(mask_blobs_1).astype(np.int)

    mask_expected_1 = np.zeros((5, 5), np.int)
    mask_expected_1[2:4, 4] = 1
    mask_expected_1[3, 3] = 1
    assert mask_main_cluster_1.tolist() == mask_expected_1.tolist()

    # TEST2: main cluster doesn't touch the boundary
    # - contains clusters touching the boundary, and not touching
    # 1 0 1 1 0 0     0 0 0 0 0 0
    # 0 0 0 0 0 0     0 0 0 0 0 0
    # 0 1 0 0 1 0 --> 0 0 0 0 1 0
    # 0 1 0 1 1 0     0 0 0 1 1 0
    # 0 0 0 0 0 0     0 0 0 0 0 0
    mask_blobs_2 = np.append(mask_blobs_1, np.zeros((5, 1), np.int), axis=1)
    mask_main_cluster_2 = utils.keep_main_cluster(mask_blobs_2).astype(np.int)

    mask_expected_2 = np.append(mask_expected_1, np.zeros((5, 1), np.int),
                                axis=1)
    assert mask_main_cluster_2.tolist() == mask_expected_2.tolist()


def test_complex_grid():
    mask = np.zeros((4, 6))
    height, width = mask.shape
    res = utils.complex_grid(mask)
    # real axis corresponds to x
    assert (np.real(res)[0, :] == (np.arange(width) - width//2)).all()
    # imaginary axis corresponds to y
    assert (np.imag(res)[:, 0] == (np.arange(height) - height//2)).all()


def test_compute_angles():
    mask = np.ones((3, 3), bool)
    mask[1, 1] = False
    angles = utils.compute_angles(mask) / np.pi
    expected_angles = np.linspace(1.0/4, 2, 8)
    assert (np.sort(angles) == expected_angles).all()

    mask = np.zeros((20, 20), bool)
    mask[3:6, 3:6] = np.ones((3, 3), bool)
    center = utils.calculate_center_mass(mask)
    mask[4, 4] = False
    angles = utils.compute_angles(mask, center=center) / np.pi
    assert (np.sort(angles) == expected_angles).all()

    angles = utils.compute_angles(mask, center=center, offset=np.pi/8) / np.pi
    diff = np.abs(np.sort(angles) - (expected_angles - 1.0/8))
    assert max(diff) < 1e-9


def test_bin_angles():
    mask = np.ones((20, 20))
    counts, bins = utils.bin_angles(mask, num_bins=8)
    # test that bins is length num_bins + 1
    assert (bins/np.pi == np.linspace(0, 2, 9)).all()
