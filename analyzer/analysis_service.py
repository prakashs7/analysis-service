import logging
import logging.handlers
import os
import os.path

import json
import glob
import pickle
import threading
import time
import traceback
import werkzeug

from tempfile import TemporaryFile
from traitlets.config.application import Application
from traitlets.config.configurable import Configurable
from traitlets import Int, Unicode

from werkzeug.wrappers import Request, Response
from werkzeug.serving import run_simple
from werkzeug.exceptions import HTTPException
from werkzeug.routing import Map, Rule

from matplotlib.image import imread

from Queue import Queue
from threading import Event

from color_utils.misc import get_timestamp

import analyzer
import analyzers
import config
import utils

from reporter import AnalysisResults
from analysis_manager import AnalysisManager
from reporter import PassiveReporter, JSONReportable


# TODO cache and logs
LOGFILE = "log.txt"
TN = 6

GET_BODY = """
<html>
<body>
<form
enctype="multipart/form-data" method="post">
<input type="text" name="service" value="analyze">
<input type="hidden" name="sessionId" value="none">
<p>
Please specify a file, or a set of files:<br>
<input type="file" name="datafile" size="40">
</p>
<div>
<input type="submit" value="Send">
</div>
</form>
</body>
</html>
"""


class AnalysisService(Application):
    config_file = Unicode(u'config.py',
                          help="Load this config file").tag(config=True)
    host = Unicode(u'0.0.0.0', help="host to to bind").tag(config=True)
    port = Int(5000, help="port to to bind").tag(config=True)
    analyzer_classes = ['Segmenter', 'Divider', 'Debug', 'Burnt', 'Spread',
                        'Decider', 'DoughType', 'PizzaGrader']

    def initialize(self, argv=None):
        logging.basicConfig(level=logging.INFO)
        logger = logging.getLogger("")
        logger.setLevel(logging.DEBUG)
        handler = logging.handlers.RotatingFileHandler(
            LOGFILE, maxBytes=(1048576*5), backupCount=7
        )
        formatter = logging.Formatter("%(asctime)s - %(name)s - "
                                      "%(levelname)s - %(message)s")
        handler.setFormatter(formatter)
        logger.addHandler(handler)
        self.parse_command_line(argv)
        try:
            self.load_config_file(self.config_file)
            logging.info('Loaded config file %s' % self.config_file)
        except Exception:
            logging.warning('Failed to load config file')

        self.analyzers = {}

        for analyzer_class in AnalysisService.analyzer_classes:
            a_class = getattr(analyzers, analyzer_class)
            self.analyzers[analyzer_class] = a_class(config=self.config)

        self.to_analyze = Queue()
        self.to_report = Queue()

    def analyze_loop(self):
        while True:
            item = self.to_analyze.get(True)

            if item == ():
                self.to_analyze.put(())
                # Termination signal
                break

            logging.info("Analyze item: %s" % item.id)
            item.reportables['item_id'] = JSONReportable(item.id)
            now_local, now_utc = get_timestamp()
            item.timestamps['begin_analysis_local'] = now_local
            item.timestamps['begin_analysis_utc'] = now_utc
            t0 = time.time()

            for name in item.analyzers:
                t0 = utils.tic()
                try:
                    logging.info("Processing %s" % name)
                    self.analyzers[name].process(item)
                except:
                    msg = "{} process on item {} failed".format(name, item.id)
                    logging.error(msg)
                    traceback.print_exc()
                item.runtimes[name] = utils.toc(t0)

            self.to_report.put(item)
            self.to_analyze.task_done()
            logging.info("Analyze done(%.2f)" % (time.time() - t0))
            now_local, now_utc = get_timestamp()
            item.timestamps['end_analysis_local'] = now_local
            item.timestamps['end_analysis_utc'] = now_utc

    def report_loop(self):
        self.reporter = PassiveReporter(config=self.config)
        while True:
            item = self.to_report.get(True)
            if item == ():
                # Termination signal
                break

            logging.info("Reporting item: %s" % item.id)
            t0 = time.time()

            item.report = self.reporter.process(item)
            item.event.set()
            self.to_report.task_done()
            logging.info("Report done(%.2f)" % (time.time() - t0))

    def start(self):
        logging.info("Analysis Service is running in process %s" % os.getpid())
        self.analyzer_threads = [threading.Thread(target=self.analyze_loop)
                                 for i in range(TN)]
        [x.start() for x in self.analyzer_threads]
        self.report_thread = threading.Thread(target=self.report_loop)
        self.report_thread.start()

        app = AnalysisServiceHandler(self)
        try:
            run_simple(self.host, self.port, app, use_debugger=False,
                       use_reloader=False)
        except KeyboardInterrupt:
            pass
        finally:
            logging.info("Terminating...")
            self.to_analyze.put(())  # Termination signal
            self.to_report.put(())  # Termination signal
            [x.join() for x in self.analyzer_threads]
            self.report_thread.join()


class AnalysisServiceHandler(Configurable):
    def __init__(self, analysis_service):
        self.analysis_service = analysis_service
        self.url_map = Map([
                Rule('/enqueueAnalysis', endpoint='enqueue_analysis'),
                Rule('/getImage', endpoint='get_image'),
                Rule('/getLastAnalysis', endpoint='get_last_analysis'),
                Rule('/isAlive', endpoint='is_alive'),
                Rule('/exit', endpoint='exit'),
                Rule('/metadata', endpoint='metadata'),
        ])

    def wsgi_app(self, environ, start_response):
        request = Request(environ)
        response = self.dispatch_request(request)
        return response(environ, start_response)

    def __call__(self, environ, start_response):
        return self.wsgi_app(environ, start_response)

    def dispatch_request(self, request):
        adapter = self.url_map.bind_to_environ(request.environ)
        try:
            endpoint, values = adapter.match()
            return getattr(self, 'on_' + endpoint)(request, **values)
        except HTTPException as e:
            return e

    def on_is_alive(self, request, **kwargs):
        return Response("yes")

    def on_metadata(self, request, **kwargs):
        content = dict(version=analyzer.version,
                       sha=analyzer.git_sha)
        return Response(json.dumps(content))

    def on_exit(self, request, **kwargs):
        func = request.environ.get('werkzeug.server.shutdown')
        if func is None:
            raise RuntimeError('Not running with the Werkzeug Server')
        func()
        return Response()

    def on_get_last_analysis(self, request, **kwargs):
        fn = sorted(glob.glob("items/*"), key=os.path.getmtime)[-1]
        obj = pickle.load(open(fn, 'rb'))
        try:
            image_id_0 = obj['report']['seg'].split('image:')[1]
        except:
            image_id_0 = ''
        try:
            image_id_1 = obj['report']['finalImage'].split('image:')[1]
        except:
            image_id_1 = ''
        body = """
<html>
<body>
<img src="getImage?imageId=%s">
<img src="getImage?imageId=%s">
<p> %s </p>
</body>
</html>
""" % (image_id_0, image_id_1, obj['report'])
        d = werkzeug.datastructures.Headers()
        d.add('Content-Type', 'text/html')
        return Response(body, headers=d)

    def on_get_image(self, request, **kwargs):
        image_id = request.args.get('imageId', '')
        if not set(image_id) <= set('0123456789abcdef'):
            return Response('Invalid <imageId>')
        if not image_id:
            return Response('<image_id> not present')
        try:
            # fn = glob.glob( 'images/%s*' % image_id)[0]
            # FIXME: likely to be broken. Fix below ? no tests :(
            fn = glob.glob('{}/{}*'.format(config.images_path, image_id))[0]
            name = fn.split("/")[-1]
            # fn = 'images/%s.jpg'%image_id

            d = werkzeug.datastructures.Headers()
            d.add('Content-Type', 'text/plain')
            d.add('Content-Disposition', 'attachment', filename=name)

            # item.report
            return Response(open(fn, 'rb').read(), headers=d)
        except:
            pass
        return Response('No such <image_id>')

    def on_enqueue_analysis(self, request, **kwargs):
        logging.info("Request start")
        t0 = time.time()
        if request.method == 'GET':
            body = GET_BODY
            d = werkzeug.datastructures.Headers()
            d.add('Content-Type', 'text/html')
            return Response(body, headers=d)
        print 'AA'
        if request.method == 'POST':
            try:
                if request.files:
                    logging.info("Got image(%.2f)" % (time.time() - t0))
                    f = request.files.values()[0].stream
                    im = imread(f, format='jpg')
                    # this raises the following error
                    # DEBUG:PIL.Image:Error closing:
                    # 'NoneType' object has no attribute 'close'
                    # This results from a bug in Pillow
                    # https://github.com/python-pillow/Pillow/issues/1144
                elif 'picPath' in request.form:
                    im = imread(request.form['picPath'], format='jpg')
                elif 'picData' in request.form:
                    data = request.form['picData']
                    with TemporaryFile() as f:
                        f.write(data)
                        f.seek(0)
                        im = imread(f, format='jpg')
                else:
                    print request.files, request.form.keys()
                    return Response("Either <picData> or "
                                    "<picPath> not present")
            except Exception as e:
                # error
                raise
                return Response("Exception: " + str(e))

            print 'A'

            # if 'analyzers' not in request.form:
            #    return Response("<analyzers> not present")
            if 'service' not in request.form:
                return Response("<service> not present")
            if request.form['service'] == 'analyze':
                analyzers = ['Segmenter', 'Divider', 'Burnt',
                             'Spread', 'Decider', 'DoughType', 'PizzaGrader']
            elif request.form['service'] == 'debug':
                analyzers = ['Segmenter', 'Divider', 'Debug']
            # elif request.form['service'] == 'beautify':
            #    analyzers = ['Segmenter', 'Toner']
            else:
                return Response("<service> not in ['analyze','beautify']")

            print 'B'
            if 'sessionId' not in request.form:
                    return Response("Either <sessionId> not present")
            session_id = request.form['sessionId']
            analyzer_list = [x for x in analyzers if x in
                             AnalysisService.analyzer_classes]
            if not analyzer_list:
                return Response('No valid analyzer')
            event = Event()
            event.clear()
            item = AnalysisResults()

            item.analyzers = AnalysisManager(analyzer_list).analyzer_list
            im = im[..., :3]
            item.analysis['im'] = im
            item.event = event
            item.request = request
            self.analysis_service.to_analyze.put(item)
            print 'c'
            event.wait()

            # d = werkzeug.datastructures.Headers()
            # d.add('Content-Type', 'text/plain')
            # d.add('Content-Disposition', 'attachment', filename='tmp.jpg')
            # imsave('tmp.jpg', item.reportables['seg'].im)
            #
            # item.report
            # return Response(open('tmp.jpg', 'rb').read(), headers=d)

            # Backup item
            logging.info("Request done(%.2f)" % (time.time()-t0))
            # t0 = time.time()
            # name = session_id + '_' + time.strftime("%y_%m_%d_%H_%M")
            # obj = {'analysis':item.analysis, 'report':item.report}
            # pickle.dump(obj, open('items/%s.pkl'%name,'wb'))
            # logging.info("Dump done(%.2f)"%(time.time()-t0))
            ret_obj = {'analysis': item.report, 'sessionId': session_id}
            return Response(json.dumps(ret_obj))


def main():
    service = AnalysisService()
    service.initialize()
    service.start()


if __name__ == '__main__':
    main()
