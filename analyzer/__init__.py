import os
import logging
import pkg_resources

import yaml

# enable subpackages
# https://packaging.python.org/guides/packaging-namespace-packages/
__path__ = __import__('pkgutil').extend_path(__path__, __name__)


THIS_DIR = os.path.dirname(__file__)

version = pkg_resources.require("analyzer")[0].version

try:
    with open(os.path.join(THIS_DIR, 'SHA'), 'r') as f:
        git_sha = f.read().strip()
except IOError:
    logging.warn("SHA file not found")
    git_sha = None


def get_premade_pizzas():
    """

    Returns:
        list[dict]: list[dict]: list of pizza recipes (name, toppings, sauce)
    """
    with open(os.path.join(THIS_DIR, 'premade_pizzas.yaml'), 'r') as yml:
        recipes = yaml.load(yml)
    return recipes
