import math
import numpy as np

from skimage import morphology

from analyzer import utils

PI = np.pi


def divide_mask(mask, num_sectors, center=None, offset=0.0, eps=1e-12):
    """ Divide mask to sectors

    Args:
        mask (np.array): a binary mask
        num_sectors (int): number of sector
        center (iterable): origin of the frame of reference
        offset (float): offset angle

    Returns:
        np.array: labels of the sector
    """
    Z = utils.complex_grid(mask, center=center) * np.exp(1j * offset)
    # make sure all angles are in the range [0, 2PI)
    angles = np.angle(Z) + PI - eps
    sector_angle = (2*PI/num_sectors)
    sector_labels = (angles / sector_angle).astype(int)
    return sector_labels + 1  # save 0 label for background


def scaling_factor(num_labels, min_val, slope):
    return np.max([min_val, 1.0 - (num_labels - 1) * slope])


# ------------------------ MORPHOLOGICAL

def mask_boundary(mask):
    return mask & ~morphology.erosion(mask, morphology.diamond(1))


def erosion(mask_in, kernel_size, split_size=39):
    """erosion with kernel size > 39 is extremely slow so we divide it into
    several erosion operations with smaller kernels.
    if kernel_size > split_size, then we do N times erosion:
    N = ceil(kernel_size / split_size), with a smaller kernel size:
    round(kernel_size/N)
    """
    if kernel_size <= split_size:
        return morphology.erosion(mask_in,
                                  np.ones((kernel_size, kernel_size), np.uint8)
                                  )

    erosion_times = int(math.ceil(float(kernel_size) / float(split_size)))

    kernel_size_small = int(0.5+kernel_size / erosion_times)
    kernel_use = np.ones((kernel_size_small, kernel_size_small), np.uint8)

    mask_out = np.copy(mask_in)
    for ind_erosion in range(0, erosion_times):
        mask_out = morphology.erosion(mask_out, kernel_use)

    return mask_out
