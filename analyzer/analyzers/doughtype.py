import logging

import enum
import numpy as np
import time

from skimage import color
from traitlets.config.configurable import Configurable

from analyzer import config
from analyzer import utils
from analyzer.reporter import JSONReportable


class DoughOptions(enum.Enum):
    cheesy = 'cheesy'
    classic = 'classic'
    thin = 'thin'


# ------------------------ CARDBOARD DETECTOR
def mode(channel, bins_range):
    """Calculate approximate mode of an image channel"""
    counts, values = np.histogram(channel.flatten(), bins_range)
    bin_centers = 0.5*(values[:-1] + values[1:])
    mode_index = np.argmax(counts)
    return bin_centers[mode_index]


def connect_corners(mask):
    return np.pad(mask, ((1, 1), (1, 1)), mode='constant', constant_values=1)


def get_mask_cardboard(image, labels, cardboard_color_lab_width):
    """ Generate a mask for the cardboard

    Args:
        image (np.array): original RGB image
        labels (np.array): pixel class labels
        cardboard_color_lab_width (np.array): width of the color peak per Lab
        color channels

    Returns:
        np.array: boolean array with the cardboard as the foreground
    """
    range_l = np.arange(0, 101)
    range_a = np.arange(-128, 128)
    range_b = np.arange(-128, 128)
    color_ranges = [range_l, range_a, range_b]

    # TODO - rgb2lab is fairly slow, explore other options
    lab_image = color.rgb2lab(image)

    background_sel = utils.topping_mask(labels, 'Background')

    channel_modes = [mode(lab_image[background_sel, channel], color_range)
                     for channel, color_range in enumerate(color_ranges)]

    cardboard_sel = np.ones(labels.shape, bool)
    for i, channel_mode in enumerate(channel_modes):
        width = cardboard_color_lab_width[i]
        channel_sel = ((channel_mode - width <= lab_image[..., i]) &
                       (lab_image[..., i] <= channel_mode + width))
        cardboard_sel &= channel_sel

    # connect the 4 parts around the pizza so that they are treated as a
    # single piece for later processing
    padded_cardboard_sel = connect_corners(cardboard_sel)

    return utils.keep_main_cluster(padded_cardboard_sel)[1:-1, 1:-1]


class DoughType(Configurable):
    depends = ["Segmenter"]

    def __init__(self, **kwargs):
        super(DoughType, self).__init__(**kwargs)
        self.dough_params = utils.load_json(config.dough_params_path)
        self.mask_cardboard = None

    def classify_dough(self, refined_crust_mask, center=None):
        """based on the crust mask and it's thickness, define the dough type.
        for classification, min and max thickness values were divided by
        nominal pizza area = 224000
        """

        crust_counts_per_angle, _ = utils.bin_angles(refined_crust_mask,
                                                     num_bins=360,
                                                     center=center)
        crust_thickness_percentiles = \
            np.percentile(crust_counts_per_angle,
                          [self.dough_params['percentile_dough_low'],
                           self.dough_params['percentile_dough_high']])

        if crust_thickness_percentiles[1] > \
                (self.dough_params['min_thickness_cheesy'] * self.pizza_area):
            return DoughOptions.cheesy.value
        elif crust_thickness_percentiles[0] < \
                (self.dough_params['max_thickness_thin'] * self.pizza_area):
            return DoughOptions.thin.value
        else:
            return DoughOptions.classic.value

    def process(self, item):
        t0 = time.time()
        logging.info('DoughType: processing item %s' % item.id)

        # FIXME: would be better to use item.analysis['label_small']
        labels = item.analysis['labels']

        mask_cardboard = \
            get_mask_cardboard(item.analysis['im_sized'], labels,
                               self.dough_params['cardboard_color_lab_width'])

        self.pizza_area = np.sum(utils.keep_main_cluster(~mask_cardboard)[:])

        crust_mask = utils.topping_mask(labels, 'Crust')

        refined_crust_mask = crust_mask & ~mask_cardboard
        center = utils.calculate_center_mass(refined_crust_mask)
        item.analysis['doughtype'] = self.classify_dough(refined_crust_mask,
                                                         center=center)

        item.analysis['mask_cardboard'] = mask_cardboard
        item.analysis['crust_mask'] = crust_mask
        item.analysis['refined_crust_mask'] = refined_crust_mask
        item.analysis['pizza_area'] = self.pizza_area
        item.reportables['doughtype'] = \
            JSONReportable(item.analysis['doughtype'])

        runtime = utils.toc(t0)
        logging.info('DoughType: done(%.2f)' % runtime)
        return item
