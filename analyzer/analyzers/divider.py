import logging

import numpy as np
import tensorflow as tf
import skimage.transform
import time

from scipy.misc import imresize
from scipy import ndimage
from skimage.measure import moments, label, regionprops
from traitlets.config.configurable import Configurable

from analyzer import utils
from analyzer.common import crop_image
from analyzer.reporter import ImageReportable, JSONReportable
from color_utils.pizza_colors import PizzaClasses
from color_utils.decider_features import non_topping_labels


pizza_classes = PizzaClasses.default().filter('name', 'Unknown')

Toppings = PizzaClasses.default().get_toppings().names
HOLE_SHIFT = [14.]*len(Toppings)
Toppings_Hole_Bound = [
  [20, 35],  # Mushrooms
  [10, 50],  # Pepperoni
  [35, 55],  # Capsicum
  [40, 60],  # Pineapple
  [50, 70],  # Jalapeno
  [60, 80],  # Anchovies
  [35, 50],  # Tomatos
]

area_count_bound = [0]*len(Toppings)
# area_count_bound[16] = 200 # Prawns
tops_to_show = [True]*len(Toppings)

# Override
Toppings_Hole_Bound = [[30, 50]]*len(Toppings)

r_cutoff = 25  # 14
a_cutoff = 0.03


# TODO: preload weights
class Divider(Configurable):
    depends = ["Segmenter"]

    def __init__(self, **kwargs):
        self.H, self.W = 256, 256
        self.AN = 48

    def normalize_persp(self, a):
        m = moments((a != 0).astype(np.uint8))
        A = m[0, 0]
        mx, my = m[0, 1] / A, m[1, 0] / A
        mxx, mxy, myy = m[0, 2]/A-mx**2, m[1, 1]/A-mx*my, m[2, 0]/A-my**2
        B = np.array([[mxx, mxy], [mxy, myy]])
        vals, vecs = np.linalg.eigh(B)
        theta = np.arccos(vecs[0, 0])
        b = skimage.transform.rotate(a, theta*180/np.pi,
                                     True, preserve_range=True,
                                     order=0).astype(np.uint8)
        l = label(b != 0)
        mask = l == np.argmax(np.bincount(l.ravel())[1:])+1
        b[~mask] = 0
        b = b[ndimage.find_objects(b)[0]]
        b = imresize(b, [self.H, self.W], interp='nearest')
        b = skimage.transform.rotate(b, -theta*180/np.pi, False,
                                     preserve_range=True,
                                     order=0).astype(np.uint8)
        return b

    def get_hole_map(self, a):
        A = a.astype(np.int32)
        for i in range(3, 256):
            r0 = np.min([A[i-1, 4:256], A[i-1, 1:253], A[i-2, 4:256],
                         A[i-2, 1:253], A[i-3, 3:255], A[i-3, 2:254]], 0)
            b = np.cumsum(A[i])[2:255]
            A[i, 3:255] = np.minimum.accumulate(r0-b[:-1])+b[1:]
            # for j in range(3,255):
            # A[i,j] = np.min([A[i,j-1], A[i-1,j+1], A[i-1,j-2], A[i-2,j+1],
            # A[i-2,j-2], A[i-3,j], A[i-3,j-1]])+A[i,j]
        return A

    def score_dist(self, seg, div, anals):
        # GAMMA = 1.
        scores = []
        origscores = []
        for j in range(len(anals)):
            anal = anals[j]
            for li in anal:
                hm = self.get_hole_map((seg >= 2) & (seg != li+3) & (div == j))
                slc = (seg >= 2) & (div == j)
                a, b = Toppings_Hole_Bound[li]
                score0 = np.log(np.mean(np.exp(hm[slc])))
                score = (score0-a)/(b-a)
                score = score*0.6 + 0.2
                score = (1-np.clip(score, 0, 1))
                scores += [score]
                origscores += [(j, Toppings[li], score, score0)]
        if not scores:
            return 0., origscores
        else:
            # return np.log(np.mean(np.exp(scores))), origscores
            return np.min(scores), origscores

    # Search 2
    def search_all(self, l0, c_0=None, DX=16, R=40):
        H, W = self.H, self.W
        # PENALTIES = [0, 0.2, 0.4, 0.6]
        PENALTIES = [0.0, 0.05, 0.6, 0.6]
        AN = self.AN
        DA = 2*np.pi/AN
        J0 = AN//8
        J1 = AN//2+1  # AN*3//8

        num_classes = np.max(l0)+1
        if c_0 is None:
            c_0 = [H/2, W/2]
        i_s, j_s = np.broadcast_arrays(*np.ogrid[:H:DX, :W:DX])
        goods = (i_s-c_0[0])**2+(j_s-c_0[1])**2 < R**2
        ci_s = i_s[goods]
        cj_s = j_s[goods]

        i_s, j_s = np.ogrid[:H, :W]

        best_score = 1e30
        best_sol = None

        for idx, (i0, j0) in enumerate(zip(ci_s, cj_s)):
            pen0 = 0.1*((i0-128)**2+(j0-128)**2)/256**2
            # shrink by degree
            angs = np.arctan2(j_s-j0, i_s-i0) % (2*np.pi)
            angs_i = (angs/DA).astype(np.int32)
            # num_angs = np.max(angs_i) + 1
            bs = np.bincount((l0 + angs_i*num_classes).ravel(),
                             minlength=num_classes*AN
                             ).reshape((AN, num_classes))[:, 2:]
            bs_ps = np.zeros((AN+1, num_classes-2), np.float32)
            np.cumsum(bs, 0, out=bs_ps[1:])

            # Calculate costs for subsequences
            # A = np.zeros((AN, AN+1), np.float32)
            i2, jump = np.ogrid[:AN, :AN+1]
            i1 = i2 - jump
            A_sums = (bs_ps[i2] - bs_ps[i1 % AN] +
                      bs_ps[-1]*((i1 < 0)[..., np.newaxis]))
            den = (np.sum(A_sums, -1).astype(np.float32)+1e-5)[..., np.newaxis]
            A = (np.sum(-np.log(A_sums / den + 1e-5) * A_sums, -1))

            # offset
            for j_off in range(AN//2):
                DP = np.zeros((5, AN + 1), np.float32) + 1e30
                back = np.zeros((5, AN + 1), np.int32)
                DP[0, 0] = 0
                # jump0 = np.r_[J0:AN]
                for i in range(1, 5):
                    for j in range(J0, AN + 1):
                        # jump = jump0[:min(j+1,J1)-J0]
                        # scores = DP[i-1,j-jump]+A[(j+j_off)%AN, jump]
                        j1 = min(j+1, J1)
                        scores = DP[i-1, j-j1+1:j-J0+1][::-1]+A[(j+j_off) %
                                                                AN, J0:j1]
                        k = np.argmin(scores)
                        DP[i, j] = scores[k]
                        back[i, j] = J0+k

                DP[1, AN] = A[0, -1]
                back[1, AN] = AN
                for i_num in range(1, 3):
                    # penalty
                    score = DP[i_num, AN]/(H*W) + PENALTIES[i_num-1] + pen0
                    if score < best_score:
                        best_score = score

                        res = []
                        i, j = i_num, AN
                        while i > 0:
                            jump = back[i, j]
                            i -= 1
                            j -= jump
                            res += [(j+j_off) % AN]
                        best_sol = ((i0, j0), res[::-1])
        return best_sol

    def count_toppings(self, r, min_bound):
        label_image = label(r)
        # S = r.shape[0]*r.shape[1]
        cnt = 0
        for region in regionprops(label_image):
            if region.area >= min_bound:
                cnt += 1
        return cnt

    def process(self, item):
        if 'labels' not in item.analysis:
            return item
        logging.info('Divider: process')
        t0 = time.time()

        l = item.analysis['labels']
        l = self.normalize_persp(l)
        num_classes = np.max(l)+1
        # li is a 3d array with a mask per label
        li = (l[..., np.newaxis] == np.r_[:num_classes]).astype(np.float32)

        # best_sol = self.search_all(l)
        best_sol = self.search_all(l, DX=8, R=1)
        c_c, c_theta = best_sol
        # best_sol = self.search_all(l, c_0=c_c, DX=3, R=8)
        best_sol = self.search_all(l, c_0=c_c, DX=8, R=1)
        c_c, c_theta = best_sol
        AN = self.AN
        c_theta = np.array(c_theta, np.float32)*2*np.pi/AN
        c_theta = np.sort(c_theta)

        i_s, j_s = np.ogrid[:self.H, :self.W]
        i_s -= int(c_c[0])
        j_s -= int(c_c[1])

        # divide
        quarter = np.zeros(li.shape[:2], np.int32)
        im = l.copy()
        n_quarters = len(c_theta)
        if len(c_theta) > 1:
            angs = (np.arctan2(j_s, i_s)) % (2*np.pi)
            for i in range(len(c_theta)-1):
                d = (angs >= c_theta[i]) & (angs < c_theta[i+1])
                quarter[d] = i+1
            for i in range(n_quarters):
                d0 = np.abs(i_s*np.sin(c_theta[i])-j_s*np.cos(c_theta[i])) < 3
                d1 = i_s*np.cos(c_theta[i])+j_s*np.sin(c_theta[i]) > 0
                d = d0 & d1  # np.abs(angs-c_theta[i])<0.05
                im[d] = 0
        im = im[..., tf.newaxis] * np.array([11, 55, 117])
        im = im.astype(np.uint8)
        im = crop_image(im, l != 0, 3)
        item.reportables['quarterImage'] = ImageReportable(im)

        # Prob areas
        pr = item.analysis['pr_small']
        i_s, j_s = np.ogrid[:pr.shape[0]-1:256j, :pr.shape[1]-1:256j]
        i_s, j_s = i_s.astype(np.int32), j_s.astype(np.int32)
        pr = pr[i_s, j_s]
        pr_areas = [np.mean(pr[..., i] > 0.5) for i in range(pr.shape[2])]

        # report
        report = {}
        anal = []
        penalty = 0.
        B = 0.002  # 0.04
        toppings = []
        areas = {}

        toppings_masks = np.delete(li.copy(), list(non_topping_labels),
                                   axis=-1)
        remove_labels = pizza_classes.select('name',
                                             ['Background', 'Crust']).labels
        pizza_cover_masks = np.delete(li.copy(), remove_labels, axis=-1)
        for i in xrange(n_quarters):
            single_res = []
            # `toppings_sums` is the count of the pixels in each channel
            toppings_sums = np.sum(toppings_masks[quarter == i, :], 0)
            pizza_cover_sums = np.sum(pizza_cover_masks[quarter == i, :], 0)
            toppings_sums /= (pizza_cover_sums.sum() + 1e-5)
            # these are not the original labels.
            anal += [np.nonzero(toppings_sums > B)[0]]
            # for i_top in np.nonzero(sums[1:] > B)[0]:
            for i_top in np.nonzero(toppings_sums > B)[0]:
                single_res += [Toppings[i_top]]
                if i_top not in toppings:
                    toppings += [i_top]
                    # the +1 is because we omit the first element
                    # areas[i_top] = sums[1 + i_top]
                    areas[i_top] = toppings_sums[i_top]

            report['q%d' % (i + 1)] = single_res
            sel = toppings_sums < B
            penalty += np.sum(toppings_sums[sel])

        dscore, dorigscores = self.score_dist(l, quarter, anal)
        penalty *= 10

        item.analysis['quarter'] = quarter
        item.analysis['l'] = l
        item.analysis['quarters'] = anal
        item.reportables['quarters'] = JSONReportable(report)

        rep_toppings = []
        for toppingIdx in toppings:
            if not tops_to_show[toppingIdx]:
                continue
            if area_count_bound[toppingIdx] > 0:
                cnt = self.count_toppings(l == (toppingIdx + 3),
                                          area_count_bound[toppingIdx])
                row = {'item': '%s (%d)' % (Toppings[toppingIdx], cnt),
                       'fill': str(areas[toppingIdx])}
            else:
                p_area = pr_areas[toppingIdx + 3]
                row = {'item': Toppings[toppingIdx] + ' (%.5f)' % p_area,
                       'fill': str(areas[toppingIdx])}
            rep_toppings.append(row)

        runtime = utils.toc(t0)
        logging.info('Divider: done(%.2f)' % runtime)
        return item
