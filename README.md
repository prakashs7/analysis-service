# AnalysisService-test1
## History and general info

* This service is designed to run in a **Linux** environment so running on 
Windows can results in all sorts of unknown and unexpected problems.

* Source code was taken from the machine in Hamilton and smashed into master.

* when we recieved the code master was written in Python 3 whereas in the 
production code (Hamilton) it is Python 2.

* You'll need AWS credentials (dev account: 2929-6437-6226) with read access 
to S3 in order to get the models

* we use camel notation in output json, first word starts with lower case

## Installation


### Production (offline installation)
0. set matplotlib backend `export MPLBACKEND=Agg`
1. Download tar file from s3: `s3://dragontail-deployment-artefacts/analysis_service/blackhole`,
 `s3://dragontail-deployment-artefacts/analysis_service/release`, from a hard drive
 or any other source

2. extract the tar `tar xfz analyzer-<version>-...-.tar.gz`
3. Create a virtual environment (Recommended) 
`virtualenv <virtual-environmene-name> && source <virtual-environmene-name>/bin/activate`
4. run the installer `./install_offline.sh`

### Development - The short way
1. Clone the repository
2. Set matplotlib backend `export MPLBACKEND=Agg` or `sudo apt-get install python-tk`
3. `sudo apt-get install curl`
4. Configure your AWS credentials for the Dev account 2929-6437-6226
5. `make fresh-install` (will create a python virtual environment for you)

### Development - The long way

1. Clone the repository
2. Create a python virtual environment (recommended)
3. One on the python dependencies is [matplotlib](https://matplotlib.org/). 
It can run with or without GUI. The service doesn't require the GUI support.
For no GUI support `export MPLBACKEND=Agg` if you want the GUI support 
`sudo apt-get install python-tk`
4. install tensorflow `./add_tensorflow_requirement.sh` 
(if that doesn't work try `chmod +x add_tensorflow_requirement.sh` 
in order to make it executable)
5. Install python requirments `pip install -r requirements.txt` . 
This script will install the GPU version if CUDA/CuDNN are installed 
and found in your systems. 
6. Install the AnalysisService package `make install` in the root directory of 
the project.
This will try and fetch the model files from s3 so you need `aws-cli` installed 
7. Run the unit tests `make unittest` in the root directory of the project
8. Run the integration test `make integration_test`

### GPU/CPU support
For utilizing the GPU you'll need to have an Nvidia GPU and 
install Nvidia drivers and CUDA, CudaDNN as described in 
[https://www.tensorflow.org/install/install_linux]() and make sure you install 
the versions specified in 
[https://dragontailsystems.atlassian.net/wiki/spaces/DTS/pages/140247045/Lenovo+Installation]()


## How to run it locally:

1. Start the service by running the `make runserver` from the `analyzer` directory. 
If sucessfull you'll get the following message in the terminal
`INFO:werkzeug: * Running on http://0.0.0.0:5000/ (Press CTRL+C to quit)`. Keep the service running.
2. Go to your browser and type `http://0.0.0.0:5000/isAlive`. This should take you to a page saying `yes`
3 There is a script to analyze all JPG images in a folder `python analyze.py --help`
**Example**
`python analyze.py <dir-with-images> --output_dir <my-output-dir>`
The results are written as JSON files in the output directory


## import the package
Once the repo is cloned you can install the package via `pip install <path-to-AnalysisService>` (normal usage) 
or `pip install -e <path-to-AnalysisService>` (for dev). Then you can use the package by importing whichever 
modules you'd like
```python
from analyzer import pizza_colors
...
```


