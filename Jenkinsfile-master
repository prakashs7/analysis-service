pipeline {
    agent { node { label 'p2xlarge' } }
    }
    stages {
        stage('Build') {
            steps {
                    sh '''
                        ls analyzer/tests/integration.py
                        git rev-parse HEAD > analyzer/SHA
                        cat analyzer/SHA
                        python -m pip install --user --upgrade pip==9.0.3
                        chmod +x add_tensorflow_requirement.sh
                        ./add_tensorflow_requirement.sh
                        pip --no-cache-dir install --user -r requirements.txt
                        export version=$(sh -c "cat VERSION")
                        export S3_MODELS_KEY=s3://dragontail-ml-models/analysis_service_models
                        export MODELS_PATH=analyzer/analyzers/models
                        aws s3 sync ${S3_MODELS_KEY}/v${version} ${MODELS_PATH}
                        pip install --user .
                        echo $PATH
                        py.test
                        ./run_integration_test.sh
                        python setup.py package
                        mv dist/*.tar.gz .
                        ls -lh
                    '''
                    stash includes: '*.tar.gz', name: 'built-tar'
                }
            }
        }
        stage('Test') {
            steps {
                    unstash 'built-tar'
                    sh '''
                        export version=$(sh -c "cat VERSION")
                        mkdir -p temp
                        ls -lh
                        mv *.tar.gz temp/ && cd temp
                        ls -lh
                        tar xzf analyzer-${version}.tar.gz
                        cd analyzer-${version}
                        pip install --user -r requirements.txt --no-index --find-links wheelhouse
                        pip install --user .
                        py.test
                        ./run_integration_test.sh
                    '''
                }
            }
        
        stage('Deployment') {
            steps {
                sshagent(['udhays-bitbucket']) {
                    unstash 'built-tar'
                    sh '''
                        export version=$(sh -c "cat VERSION")
                        pip install --user awscli
                        export branch=master
                        export githash=$(git rev-parse --short HEAD)
                        artifactVersion=analyzer-${version}-${branch}-${githash}
                        sed -i "s/<build-tar-name>/analyzer-${version}/g" install_offline.sh
                        ls -lh
                        tar czf ${artifactVersion}.tar.gz analyzer-${version}.tar.gz install_offline.sh
                        mkdir -p temp
                        cp ${artifactVersion}.tar.gz temp/
                        cd temp
                        ls -lh
                        tar xzf ${artifactVersion}.tar.gz
                        ls -lh
                        ./install_offline.sh
                        cd ..
                        echo aws s3 cp ${artifactVersion}.tar.gz s3://dragontail-deployment-artefacts/analysis_service/releases/
                    '''
                }
            }
        }
    }
    post {
        always {
            sh 'echo Finished pipeline'
        }
        failure {
            sh 'echo An error occurred'
        }
    }

